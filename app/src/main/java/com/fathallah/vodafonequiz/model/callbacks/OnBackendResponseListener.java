package com.fathallah.vodafonequiz.model.callbacks;

/**
 * Created by Ahmed on 11/6/2015.
 */
public interface OnBackendResponseListener {

    void onSuccess(Object object);

    void onError(Exception e);
}
