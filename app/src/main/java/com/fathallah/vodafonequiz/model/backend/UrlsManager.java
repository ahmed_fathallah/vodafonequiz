package com.fathallah.vodafonequiz.model.backend;


public class UrlsManager {

    public static final String BASE_URL = "https://api.flickr.com/services/rest/?";
    public static final String FLICKER_API_KEY = "c7e4e97dc0cc239fa41e38364ef50a2b";
    public static String JSON = "&format=json&nojsoncallback=1";


    public static String createSearchPhotosURL(String tag, int numPerPage, int page) {

        String url = BASE_URL + "method=flickr.photos.search"
                + "&api_key=" + FLICKER_API_KEY
                + "&tags=" + tag
                + "&per_page=" + numPerPage
                + "&page=" + page
                + JSON;

        return url;

    }

    public static String createPhotosForUserURL(String userId, int numPerPage, int page) {

        String url = BASE_URL + "method=flickr.photos.search"
                + "&api_key=" + FLICKER_API_KEY
                + "&user_id=" + userId
                + "&per_page=" + numPerPage
                + "&page=" + page
                + JSON;
        return url;

    }

}
