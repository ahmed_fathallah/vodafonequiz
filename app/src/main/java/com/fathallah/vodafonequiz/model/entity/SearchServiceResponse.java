package com.fathallah.vodafonequiz.model.entity;

import com.google.gson.annotations.SerializedName;


public class SearchServiceResponse {

    @SerializedName("photos")
    private PhotosPage photosPage;
    @SerializedName("stat")
    private String stat;


    public PhotosPage getPhotosPage() {
        return photosPage;
    }

    public void setPhotosPage(PhotosPage photosPage) {
        this.photosPage = photosPage;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }
}
