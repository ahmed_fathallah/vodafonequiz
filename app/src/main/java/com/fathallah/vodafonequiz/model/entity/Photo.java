package com.fathallah.vodafonequiz.model.entity;

/**
 * Created by Ahmed.PC on 11/4/2015.
 */
public class Photo {

    private String id;

    private String owner;

    private String secret;

    private String server;

    private int farm;

    private String title;

    private int ispublic;

    private int isfriend;

    private int isfamily;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner The owner
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return The secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * @param secret The secret
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * @return The server
     */
    public String getServer() {
        return server;
    }

    /**
     * @param server The server
     */
    public void setServer(String server) {
        this.server = server;
    }

    /**
     * @return The farm
     */
    public int getFarm() {
        return farm;
    }

    /**
     * @param farm The farm
     */
    public void setFarm(int farm) {
        this.farm = farm;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The ispublic
     */
    public int getIspublic() {
        return ispublic;
    }

    /**
     * @param ispublic The ispublic
     */
    public void setIspublic(int ispublic) {
        this.ispublic = ispublic;
    }

    /**
     * @return The isfriend
     */
    public int getIsfriend() {
        return isfriend;
    }

    /**
     * @param isfriend The isfriend
     */
    public void setIsfriend(int isfriend) {
        this.isfriend = isfriend;
    }

    /**
     * @return The isfamily
     */
    public int getIsfamily() {
        return isfamily;
    }

    /**
     * @param isfamily The isfamily
     */
    public void setIsfamily(int isfamily) {
        this.isfamily = isfamily;
    }


    public String getPhotoURL() {
        String url = "http://farm" + farm + ".static.flickr.com/"
                + server + "/" + id + "_" + secret + "_m.jpg";
        return url;
    }
}


