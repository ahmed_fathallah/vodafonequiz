package com.fathallah.vodafonequiz.model.backend;

import com.android.volley.Cache;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fathallah.vodafonequiz.model.callbacks.OnBackendResponseListener;
import com.fathallah.vodafonequiz.model.entity.SearchServiceResponse;
import com.fathallah.vodafonequiz.utils.ConnectionDetector;
import com.fathallah.vodafonequiz.utils.QuizApplication;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

/**
 * class contains all services of get PhotosList
 */
public class PhotosService {


    private OnBackendResponseListener mOnBackendResponseListener;


    public void getPhotosPerUser(String userId, int perPage, int page
            , OnBackendResponseListener onBackendResponseListener) {
        mOnBackendResponseListener = onBackendResponseListener;
        String url = UrlsManager.createPhotosForUserURL(userId, perPage, page);
        String cachedData = getRequestCached(url);
        if (cachedData == null) {
            getPhotosFromService(url);
        } else {
            SearchServiceResponse response = new Gson().fromJson(cachedData, SearchServiceResponse.class);
            mOnBackendResponseListener.onSuccess(response);
        }
    }

    public void getPhotosForTag(String tag, int perPage, int page
            , OnBackendResponseListener onBackendResponseListener) {
        this.mOnBackendResponseListener = onBackendResponseListener;
        String url = UrlsManager.createSearchPhotosURL(tag, perPage, page);
        String cachedData = getRequestCached(url);
        if (cachedData == null) {
            getPhotosFromService(url);
        } else {
            SearchServiceResponse response = new Gson().fromJson(cachedData, SearchServiceResponse.class);
            mOnBackendResponseListener.onSuccess(response);
        }

    }

    private void getPhotosFromService(String url) {

        GsonRequest<SearchServiceResponse> photoGsonRequest = new GsonRequest<>(url, SearchServiceResponse.class,
                null,
                new Response.Listener<SearchServiceResponse>() {
                    @Override
                    public void onResponse(SearchServiceResponse response) {
                        mOnBackendResponseListener.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mOnBackendResponseListener.onError(error);
            }
        });

        QuizApplication.getInstance().getRequestQueue().add(photoGsonRequest);
    }


    private String getRequestCached(String url) {
        Cache cache = QuizApplication.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if (entry != null) {
            try {
                String data = new String(entry.data, "UTF-8");
                return data;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
