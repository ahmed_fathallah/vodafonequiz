package com.fathallah.vodafonequiz.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.fathallah.vodafonequiz.R;
import com.fathallah.vodafonequiz.model.backend.PhotosService;
import com.fathallah.vodafonequiz.model.callbacks.OnBackendResponseListener;
import com.fathallah.vodafonequiz.model.entity.SearchServiceResponse;
import com.fathallah.vodafonequiz.ui.fragments.UserPhotosFragment;

public class UserPhotoesActivity extends AppCompatActivity {


    public static final String USER_ID = "userId";
    private UserPhotosFragment mUserPhotosFragment;
    private ProgressBar mProgressBar;
    private PhotosService photosService;
    private String userId;
    private int page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_photoes);

        userId = getIntent().getStringExtra(USER_ID);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgressBar = (ProgressBar) findViewById(R.id.circularProgressbar);


        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        mUserPhotosFragment = (UserPhotosFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_photo);

        photosService = new PhotosService();

        getUserPhotos();

    }

    private void getUserPhotos() {
        photosService.getPhotosPerUser(userId, 30, page, new OnBackendResponseListener() {
            @Override
            public void onSuccess(Object object) {
                SearchServiceResponse response = (SearchServiceResponse) object;
                mProgressBar.setVisibility(View.GONE);
                mUserPhotosFragment.setPhotoList(response.getPhotosPage().getPhotoList());
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                mProgressBar.setVisibility(View.GONE);
                if(e instanceof NoConnectionError){
                    Snackbar.make(findViewById(R.id.layout), R.string.no_connection, Snackbar.LENGTH_SHORT).show();
                }else{
                    Snackbar.make(findViewById(R.id.layout), R.string.error_happened, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }
}
