package com.fathallah.vodafonequiz.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fathallah.vodafonequiz.R;
import com.fathallah.vodafonequiz.adapters.PhotosListAdapter;
import com.fathallah.vodafonequiz.model.entity.Photo;

import java.util.ArrayList;


public class UserPhotosFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private PhotosListAdapter mPhotosListAdapter;
    private ArrayList<Photo> photos;
    private View view;

    private OnPhotoSelectedListener mOnPhotoSelectedListener;

    public UserPhotosFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_photoes, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mPhotosListAdapter = new PhotosListAdapter();
        mRecyclerView.setAdapter(mPhotosListAdapter);
        photos = new ArrayList<>();
        mPhotosListAdapter.setonItemClickListener(new PhotosListAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int position) {
                if (mOnPhotoSelectedListener != null)
                    mOnPhotoSelectedListener.onPhotoSelected(photos.get(position).getOwner());
            }
        });
    }

    public void setPhotoList(ArrayList<Photo> photoList) {

        photos.addAll(photoList);
        mPhotosListAdapter.addPhotos(photoList);

    }

    public void clearPhotosList() {
        mPhotosListAdapter.clearAllPhotos();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            if ((context) instanceof OnPhotoSelectedListener) {
                mOnPhotoSelectedListener = (OnPhotoSelectedListener) context;
            }
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mOnPhotoSelectedListener = null;
    }

    public interface OnPhotoSelectedListener {
        void onPhotoSelected(String owner);
    }
}
