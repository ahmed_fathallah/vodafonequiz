package com.fathallah.vodafonequiz.ui.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.fathallah.vodafonequiz.R;
import com.fathallah.vodafonequiz.model.backend.PhotosService;
import com.fathallah.vodafonequiz.model.callbacks.OnBackendResponseListener;
import com.fathallah.vodafonequiz.model.entity.SearchServiceResponse;
import com.fathallah.vodafonequiz.ui.fragments.UserPhotosFragment;

public class HomeActivity extends AppCompatActivity implements UserPhotosFragment.OnPhotoSelectedListener {

    private UserPhotosFragment userPhotosFragment;
    private ProgressBar mProgressBar;
    private PhotosService photosService;
    private int page =1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        userPhotosFragment = (UserPhotosFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_photo);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgressBar = (ProgressBar) findViewById(R.id.circularProgressbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        photosService = new PhotosService();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint("Type something...");
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setMaxWidth(Toolbar.LayoutParams.MATCH_PARENT);
        searchView.setIconified(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String tag) {
                mProgressBar.setVisibility(View.VISIBLE);
                userPhotosFragment.clearPhotosList();
                searchForImages(tag);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {

                return false;
            }
        });
        return true;
    }

    private void searchForImages(String tag) {

        photosService.getPhotosForTag(tag, 30, page, new OnBackendResponseListener() {
            @Override
            public void onSuccess(Object object) {
                SearchServiceResponse response = (SearchServiceResponse) object;
                mProgressBar.setVisibility(View.GONE);
                userPhotosFragment.setPhotoList(response.getPhotosPage().getPhotoList());
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                mProgressBar.setVisibility(View.GONE);
                if(e instanceof NoConnectionError){
                    Snackbar.make(findViewById(R.id.layout),R.string.no_connection,Snackbar.LENGTH_SHORT).show();
                }else{
                    Snackbar.make(findViewById(R.id.layout), R.string.error_happened, Snackbar.LENGTH_SHORT).show();
                }

            }
        });
    }


    @Override
    public void onPhotoSelected(String owner) {
        Intent intent = new Intent(HomeActivity.this, UserPhotoesActivity.class);
        intent.putExtra(UserPhotoesActivity.USER_ID, owner);
        startActivity(intent);

    }
}
