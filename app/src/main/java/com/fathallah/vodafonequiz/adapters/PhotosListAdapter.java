package com.fathallah.vodafonequiz.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fathallah.vodafonequiz.R;
import com.fathallah.vodafonequiz.model.entity.Photo;
import com.fathallah.vodafonequiz.utils.QuizApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter Of  Photos List
 */
public class PhotosListAdapter extends RecyclerView.Adapter<PhotosListAdapter.ViewHolder> {


    private List<Photo> photoList;

    private OnItemClickListener mOnItemClickListener;

    public PhotosListAdapter() {

        photoList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_photo, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photo photo = photoList.get(position);

        holder.titleTextView.setText(photo.getTitle());
        ImageLoader imageLoader = QuizApplication.getInstance().getImageLoader();
        holder.image.setImageUrl(photo.getPhotoURL(), imageLoader);


    }

    @Override
    public int getItemCount() {
        if (photoList == null)
            return 0;
        return photoList.size();
    }

    public void addPhotos(List<Photo> photos) {
        if (photoList == null)
            photoList = new ArrayList<>();
        photoList.addAll(photos);
        this.notifyDataSetChanged();
    }

    public void setonItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void clearAllPhotos() {
        photoList.clear();
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClicked(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public NetworkImageView image;
        public TextView titleTextView;

        public ViewHolder(final View itemView) {
            super(itemView);
            image = (NetworkImageView) itemView.findViewById(R.id.imv_photo);
            titleTextView = (TextView) itemView.findViewById(R.id.tv_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClicked(v, getAdapterPosition());
                }
            });
        }
    }
}
